package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utility.BaseClass;

public class AutoReg extends BaseClass {
	public WebElement element;
	public static  final String Sign_name="//a[@class='login']";
	public static  final String UserName_name="email_create";
	public static  final String Submit_name="SubmitCreate";
	
	public AutoReg(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public WebElement Signin()
	{
			
		element = getElementXpath(Sign_name);
			return element;		
	}
	public WebElement UserName()
	{
		element = getElementId(UserName_name);
		
		return element;
	}
	public WebElement Submit()
	{
			
		element = getElementName(Submit_name);
			return element;		
	}
	
	

}
