package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utility.BaseClass;

public class YahooReg extends BaseClass {

	
	public WebElement element;
	public static  final String FirstName_name="firstName";
	public static  final String LastName_name="lastName";
	public static  final String UserName_name="yid";
	public static  final String Password_name="password";
	public static  final String Phone_name="phone";
	public static  final String Month_name="mm";
	public static  final String Day_name="dd";
	public static  final String Year_name="yyyy";
	public static  final String Gender_id="//input[@id='usernamereg-freeformGender']";
	public static  final String Acknowledge_name="signup";
	public YahooReg(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

		public WebElement FirstName()
	{
		element = getElementName(FirstName_name);
		
		return element;
	}
	public WebElement LastName()
	{
		element = getElementName(LastName_name);
		
		return element;
	}
	
	public WebElement UserName()
	{
		element = getElementName(UserName_name);
		
		return element;
	}
	public WebElement Password()
	{
		element = getElementName(Password_name);
		
		return element;
	}
	public WebElement Phone()
	{
		element = getElementName(Phone_name);
		
		return element;
	}
	public Select Month()
	{
			
		Select month = new Select(getElementName(Month_name));
		
			return month;		
	}
	public WebElement Day()
	{
			
		element = getElementName(Day_name);
			return element;		
	}
	public WebElement Year()
	{
			
		element = getElementName(Year_name);
			return element;		
	}
	public WebElement Gender()
	{
			
		element = getElementXpath(Gender_id);
			return element;		
	}
	public WebElement Acknowledge()
	{			
		element = getElementName(Acknowledge_name);
			return element;		
	}
}
