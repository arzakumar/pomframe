package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utility.BaseClass;

public class CreateAcctPage extends BaseClass {
	public WebElement element;
	public static  final String Radio1="(//input[@type='radio'])[1]";
			//"//input[@type='radio'][1]";
	public static  final String Radio2="uniform-id_gender2";
	public static  final String FirstName_name="customer_firstname";
	public static  final String LastName_name="customer_lastname";
	public static  final String Password_name="passwd";
	public static  final String Day_name="//select[@id='days']";
	public static  final String Month_name="//select[@id='months']";
	public static  final String Year_name="//select[@id='years']";
	public static  final String Check1="//div[@id='uniform-newsletter']";
	public static  final String Company_name="//input[@id='company']";
	public static  final String Address1_name="//input[@id='address1']";
	public static  final String Address2_name="//input[@id='address2']";
	public static  final String City_name="//input[@id='city']";
	public static  final String State_name="//select[@id='id_state']";

	
	
	
	public CreateAcctPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public WebElement Radio1_name()
	{
		element = getElementXpath(Radio1);
		
		return element;
	}
	/*public WebElement Radio2_name()
	{
		element = getElementId(Radio2);
		
		return element;
	}
	*/
	public WebElement FirstName()
{
	element = getElementId(FirstName_name);
	
	return element;
}
public WebElement LastName()
{
	element = getElementName(LastName_name);
	
	return element;
}

public WebElement Password()
{
	element = getElementName(Password_name);
	System.out.println(element);

	return element;
}

public WebElement Day()
{
	System.out.println(Day_name);
   element =getElementXpath("//div[@id='uniform-days']");
	return element;
         		
}
public Select Month()
{
	System.out.println(Month_name);

	Select month1 = new Select(getElementXpath(Month_name));
	System.out.println(month1);
		return month1;	
		
}
public Select Year()
{
	Select year1 = new Select(getElementXpath(Year_name));
	return year1;		
}

public WebElement Check1_name()
{
	element = getElementXpath(Check1);
	
	return element;
}
public WebElement CompanyName()
{
element = getElementXpath(Company_name);

return element;
}
public WebElement Address1Name()
{
element = getElementXpath(Address1_name);

return element;
}
public WebElement Address2Name()
{
element = getElementXpath(Address2_name);

return element;
}
public WebElement CityName()
{
element = getElementXpath(City_name);

return element;
}

public Select State()
{
	System.out.println(State_name);
	Select state1 =new Select(getElementXpath(State_name));
	return state1;
         		
}
}
