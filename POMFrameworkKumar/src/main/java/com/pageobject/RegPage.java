package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utility.BaseClass;

public class RegPage extends BaseClass  {
	public WebElement element;
	public static  final String FirstName_name="firstname";
	public static  final String LastName_name="lastname";
	public static  final String MobileEmail_xpath="//*[@id=\"u_9_g_E6\"]";
	public static  final String Password_name="reg_passwd__";
	public static  final String Birthday_day="birthday_day";
	public static  final String Birthday_month="birthday_month";
	public static  final String Birthday_year="birthday_year";
	

	public RegPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public WebElement FirstName()
	{
		element = getElementName(FirstName_name);
		
		return element;
	}
	public WebElement LastName()
	{
		element = getElementName(LastName_name);
		
		return element;
	}
	public WebElement MobileEmail()
	{
		element = getElementXpath(MobileEmail_xpath);
		
		return element;
	}
	public WebElement UserPassword()
	{
		element = getElementName(Password_name);
		
		return element;
	}
	public WebElement BirthDay()
	{
		element = getElementName(Birthday_day);
		
		return element;
	}
	public WebElement BirthMonth()
	{
		element = getElementName(Birthday_month);
		
		return element;
	}
	public WebElement BirthYear()
	{
		element = getElementName(Birthday_year);
		Select  = new Select(element);
				
		return element;
	}
	
}
