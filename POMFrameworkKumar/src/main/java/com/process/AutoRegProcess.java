package com.process;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.pageobject.AutoReg;
import com.pageobject.CreateAcctPage;

import utility.BaseClass;



public class AutoRegProcess extends BaseClass {

	public AutoRegProcess(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	AutoReg ag = new AutoReg(driver);
	CreateAcctPage cp = new CreateAcctPage(driver);
	
	public void Sign()
	{
		String username = ag.RandomStringGeneration();
		username="username"+"@yahoo.com";
				
		ag.Signin().click(); 
		System.out.println("Clicked the Signin Button");
		ag.UserName().sendKeys(username);
		System.out.println(username);
		ag.Submit().click();
		System.out.println("Clicked the Submit Button");
	}
	
	public void Create() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	
		cp.Radio1_name().click();
		//cp.FirstName().sendKeys("Kumar");
		System.out.println("Firstname");
		//cp.LastName().sendKeys("Arza");
		System.out.println("Lastname");
		cp.Password().sendKeys("12345fdfdfd4");
		cp.Password().sendKeys(Keys.TAB);
		
		Thread.sleep(4000);
		cp.Day().click();
		cp.Day().sendKeys(Keys.ARROW_DOWN);
		cp.Day().sendKeys(Keys.ENTER);
		
		//  Select sel = new Select(cp.Day()); sel.selectByValue("10");
		  
		  cp.Month().selectByValue("10"); cp.Month().selectByVisibleText("February");
		  cp.Year().selectByValue("2000"); cp.Check1_name().click();
		 
		cp.CompanyName().sendKeys("Wipro");
		cp.Address1Name().sendKeys("Hitech City");
		cp.Address2Name().sendKeys("Central");
		cp.CityName().sendKeys("Hyderabad");
		cp.State().selectByValue("5");
		
		
	}	
	
	

}
