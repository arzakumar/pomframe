package utility;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class BaseClass {
	
	public static WebDriver driver;
	public String RandomStr;
	protected static Wait<WebDriver> wait;
	
	
	public BaseClass(WebDriver driver){
		
		BaseClass.driver = driver;
		wait = new FluentWait<WebDriver>(driver)
				.withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(20))
				.ignoring(NoSuchElementException.class);
		
	}
	public String RandomStringGeneration() {
		// TODO Auto-generated method stub
		
		RandomStringGenerator randomStringGenerator =
		        new RandomStringGenerator.Builder()
		                .withinRange('0', 'z')
		                .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
		                .build();
		System.out.println(randomStringGenerator.generate(6));
		return randomStringGenerator.generate(6);
	
}

	protected static WebElement getElementId(String ElementLocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ElementLocator)));
	}
	protected static WebElement getElementName(String ElementLocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(ElementLocator)));
	}
	
	protected static WebElement getElementXpath(String ElementLocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ElementLocator)));
	}
	protected static WebElement getElementclass(String ElementLocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(ElementLocator)));
	}
	
	protected static WebElement getElementLinkText(String ElementLocator)
	{
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(ElementLocator)));
	}
	
	 


}
