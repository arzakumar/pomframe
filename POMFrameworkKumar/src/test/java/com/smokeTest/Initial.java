package com.smokeTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;

public class Initial {
	WebDriver driver;
	FileInputStream fis;
	Properties prop = new Properties();
	@BeforeClass
	public void Initalization() throws IOException
	{
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
		 fis = new FileInputStream(System.getProperty("user.dir")+"\\Property\\config.properties");
		prop.load(fis);
		driver = new ChromeDriver();//run time polymorphism
		
		//webdriver - interface 
		driver.get(prop.getProperty("QA_URL"));
		driver.manage().window().maximize();
		
		System.out.println(driver.getTitle());
	}

}
